import axios from 'axios'

export default {
  namespaced: true,
  state: {
  	token:'',
  	user: {
  		userName: '',
  		password: ''
  	}
  },
  mutations: {
  	setToken (state, token) {
  		state.token = token
  	},
  	setUser (state, data) {
  		state.user = data
  	}
  },

  actions: {
  	async logIn({dispatch}, credentials) {
	    let response = await axios.post('auth/login', credentials);
	    dispatch('attempt', response.data.token);
	},
	async attempt ({commit}, token) {
		
		commit('setToken', token);

		try {
			let response = await axios.get('auth/ticket', {
				headers: {
					'Authorization': 'Token: ' + token
				}
			});
			console.log(response)
			commit('setUser', response.data)
		} catch(e) {
			console.log('failed')
		}
	}
  },
  
}
