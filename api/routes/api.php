<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'auth',  'namespace' => 'App\Http\Controllers\Auth'], function () {
	Route::post('login', 'LogInController');
	Route::post('logout', 'LogOutController');
	Route::get('me', 'MeController');
	Route::get('ticket', 'TicketController');
});

// Route::get('register', 'RegisterController');