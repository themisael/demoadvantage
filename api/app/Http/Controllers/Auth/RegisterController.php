<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    //
    public function _construct() {

          $this->middleware(['auth:api']);

     }

    public function __invoke(Request $request) {
    	
        $request->validate([
            'userName' => ['required', 'unique:users'],
            'name' => ['required'],
            'password' => ['required', 'min:6'],
        ]);

        User::create([
            'userName' => $request->userName,
            'name' => $request->name,
            'phone' => $request->phone,
            'birthDate' => $request->birthDate,
            'password' => Hash::make($request->password),
        ]);
        
    }
}
