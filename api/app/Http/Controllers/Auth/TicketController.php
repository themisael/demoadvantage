<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    //
	public function _construct() {

		$this->middleware(['auth:api']);

	}

    public function __invoke( Request $request) {
    	return response()->json([
    		'userName' => $request->only('userName'),
    		'password' => $request->only('password')
    	]);
    	
    }
}
